const ROLES_LIST = {
    "Managers": "Managers",
    "Employee": "Employee",
    "Admin":   "Admin",
    "Editor": "Editor",
    "User": "User"
}

module.exports = ROLES_LIST