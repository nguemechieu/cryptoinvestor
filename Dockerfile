FROM ubuntu:latest

RUN echo mkdir "cryptoinvestor"
RUN echo cd "cryptoinvestor"
FROM alpine:latest
ENTRYPOINT echo "cryptoinvestor"
RUN mkdir ".bin"
RUN echo cd ".bin"

WORKDIR .bin
COPY ./package.json ./
RUN npm install
COPY . .
EXPOSE 4000
CMD ["npm", "run", "production"]


